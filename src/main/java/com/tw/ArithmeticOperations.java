package com.tw;
public class ArithmeticOperations {
    int number1;
    int number2;
    public ArithmeticOperations(int number1, int number2){
        this.number1= number1;
        this.number2= number2;
    }
    public int add() {
        return this.number1 + this.number2;
    }

    public int difference() {
        return this.number1-this.number2;
    }

    public int multiply() {
        return this.number1 * this.number2;
    }

    public int divide() {
        return this.number1/this.number2;
    }
}
