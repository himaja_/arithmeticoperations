package com.tw;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ArithmeticOperationsTest {

    ArithmeticOperations arithmetic;

    @Test
    void shouldReturnSumWhenPositiveIntegersAreGiven(){
        arithmetic= new ArithmeticOperations(2, 3);
        int actualValue= arithmetic.add();
        int expectedValue= 5;
        assertThat(expectedValue, is(equalTo(actualValue)));
    }

    @Test
    void shouldReturnSumWhenNegativeIntegersAreGiven(){
        arithmetic= new ArithmeticOperations(-2, -3);
        int actualValue= arithmetic.add();
        int expectedValue= -5;
        assertThat(expectedValue, is(equalTo(actualValue)));
    }

    @Test
    void shouldReturnDifferenceWhenPositiveIntegersAreGiven(){
        arithmetic= new ArithmeticOperations(2, 3);
        int actualValue= arithmetic.difference();
        int expectedValue= -1;
        assertThat(expectedValue, is(equalTo(actualValue)));
    }

    @Test
    void shouldReturnDifferenceWhenNegativeIntegersAreGiven(){
        arithmetic= new ArithmeticOperations(-2, -3);
        int actualValue= arithmetic.difference();
        int expectedValue= 1;
        assertThat(expectedValue, is(equalTo(actualValue)));
    }

    @Test
    void shouldReturnProductWhenTwoPositiveIntegersAreGiven(){
        arithmetic= new ArithmeticOperations(2, 3);
        int actualValue= arithmetic.multiply();
        int expectedValue= 6;
        assertThat(expectedValue, is(equalTo(actualValue)));
    }

    @Test
    void shouldReturnProductWhenTwoNegativeIntegersAreGiven(){
        arithmetic= new ArithmeticOperations(-2, -3);
        int actualValue= arithmetic.multiply();
        int expectedValue= 6;
        assertThat(expectedValue, is(equalTo(actualValue)));
    }

    @Test
    void shouldReturnProductWhenOnePositiveAndOneNegativeAreGiven(){
        arithmetic= new ArithmeticOperations(2, -3);
        int actualValue= arithmetic.multiply();
        int expectedValue= -6;
        assertThat(expectedValue, is(equalTo(actualValue)));
    }

    @Test
    void shouldReturnZeroWhenOneIsZeroAndOnePositiveAreGiven(){
        arithmetic= new ArithmeticOperations(0, 2);
        int actualValue= arithmetic.multiply();
        int expectedValue= 0;
        assertThat(expectedValue, is(equalTo(actualValue)));
    }

    @Test
    void shouldReturnDivisionWhenTwoPositiveIntegersAreGiven(){
        arithmetic= new ArithmeticOperations(4, 2);
        int actualValue= arithmetic.divide();
        int expectedValue= 2;
        assertThat(expectedValue, is(equalTo(actualValue)));
    }

    @Test
    void shouldReturnDivisionWhenTwoNegativeIntegersAreGiven(){
        arithmetic= new ArithmeticOperations(-4, -2);
        int actualValue= arithmetic.divide();
        int expectedValue= 2;
        assertThat(expectedValue, is(equalTo(actualValue)));
    }

    @Test
    void shouldReturnZeroWhenZeroIsDividedByAnyOtherInteger(){
        arithmetic= new ArithmeticOperations(0, -2);
        int actualValue= arithmetic.divide();
        int expectedValue= 0;
        assertThat(expectedValue, is(equalTo(actualValue)));
    }

    @Test
    void shouldReturnZeroDivideExceptionWhenDividedByZero(){
        arithmetic= new ArithmeticOperations(2, 0);
        Assertions.assertThrows(ArithmeticException.class, () -> {
            int actualValue = arithmetic.divide();
        });
    }

}
